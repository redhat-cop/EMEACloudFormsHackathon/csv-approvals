class FillDropdown
  def dropdown_values
    csv_content = $evm.get_state_var(:csv_content) || []

    # TODO: make unique
    values = csv_content.each_with_object({}) { |line, acc| acc[line[:project]] = "#{line[:project]}" }

    $evm.object['sort_by']       = 'description'
    $evm.object['sort_order']    = 'ascending'
    $evm.object['data_type']     = 'string'
    $evm.object['required']      = 'true'
    $evm.object['values']        = values
    $evm.object['default_value'] = values.first
  end
end

#
# Main
#
# This automate method is meant to be used to dynamically populate ServiceDialog dropdown with available project names.
# This automate method depends on :csv_content state attribute being set to following format:
# [
#   {
#     :first_name => String,
#     :last_name  => String,
#     :username   => String,
#     :email      => String,
#     :project    => String
#   },
#   ...
# ]
#

if __FILE__ == $PROGRAM_NAME
  FillDropdown.new.dropdown_values
end
