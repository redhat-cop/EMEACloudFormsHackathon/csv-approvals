class CSVTagImport
  attr :csv_content, :file_conf

  def initialize()
    @csv_content = $evm.get_state_var(:csv_content) || []

    @file_conf = {}
  end

  def set_file_conf(category_name:,category_description:)
    @file_conf = {
      :category_name         => category_name,
      :category_description  => category_description
    }
    self
  end

  # List all the project tags from the CSV file, and put them into an Array
  def get_project_tags()
    list_of_tags = []
    @csv_content.each do |line|
      list_of_tags.push(line[:project])
    end

    list_of_tags.uniq
  end

  # Define all the project tags in MS Azure, by grabbing the tags from CSV
  def populate_project_tags()

    unless $evm.execute('category_exists?', @file_conf[:category_name])
      $evm.execute('category_create', :name => @file_conf[:category_name],
      :single_value => true,
      :description => @file_conf[:category_description])
      $evm.log(:info,"Adding tag category: #{@file_conf[:category_name]}")
    end

    get_project_tags().each do |record|
      # The tag name needs to be lowercase, otherwise the function will error out
      tag_name = record.downcase

      # TODO: Maybe have another column, with a more understandable description. So
      #       that we can understand what the project code is for
      tag_display_name = record.force_encoding(Encoding::UTF_8)

      unless $evm.execute('tag_exists?', @file_conf[:category_name], tag_name)
        $evm.execute('tag_create', @file_conf[:category_name], :name => tag_name,
                     :description => tag_display_name)
        $evm.log(:info,"Adding tag '#{tag_name}' to category '#{@file_conf[:category_name]}'")
      end
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  importer = CSVTagImport.new

  importer.set_file_conf(
    :category_name        => $evm.object['cf_category_name'],
    :category_description => $evm.object['cf_category_description']
  )

  importer.populate_project_tags
end
